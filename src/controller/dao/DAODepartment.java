/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Department;
import model.Employee;

/**
 *
 * @author Cuong Pham
 */
public class DAODepartment extends IDAO<Department> {

    public DAODepartment(Connection conn) {
        this.conn = conn;
        try {
            this.statement = this.conn.createStatement();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Department> selectAll() {
        List<Department> result = new ArrayList<>();
        try {
            String sql = "SELECT * FROM DEPARTMENT";

            rs = statement.executeQuery(sql);
            while (rs.next()) {
                Department d = new Department(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
                result.add(d);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        return result;
    }

    @Override
    public List<Department> selectByName(String name) {
        List<Department> result = new ArrayList<>();
        try {
            String sql = "SELECT * FROM DEPARTMENT WHERE DEPT_NAME LIKE '%" + name + "%'";

            rs = statement.executeQuery(sql);
            while (rs.next()) {
                Department d = new Department(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
                result.add(d);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        return result;
    }

    @Override
    public int insert(Department department) {
        String sql = "INSERT INTO DEPARTMENT (DEPT_NAME, DEPT_NO, LOCATION) VALUES (?,?,?)";
        try {
            this.preStatement = this.conn.prepareStatement(sql);
            this.preStatement.setString(1, department.getDeptName());
            this.preStatement.setString(2, department.getDeptNo());
            this.preStatement.setString(3, department.getLocation());
            
            int rowCount = this.preStatement.executeUpdate();
            return rowCount;
        } catch (SQLException ex) {
            Logger.getLogger(DAODepartment.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public int update(Department department) {
        String sql = "UPDATE DEPARTMENT SET DEPT_NAME = ?, DEPT_NO = ?, LOCATION = ? WHERE DEPT_ID = ?";
        try {
            this.preStatement = this.conn.prepareStatement(sql);
            this.preStatement.setString(1, department.getDeptName());
            this.preStatement.setString(2, department.getDeptNo());
            this.preStatement.setString(3, department.getLocation());
            this.preStatement.setInt(4, department.getDeptId());
            
            int rowCount = this.preStatement.executeUpdate();
            return rowCount;
        } catch (SQLException ex) {
            Logger.getLogger(DAODepartment.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public void closeConnection() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int delete(int id) {
        try {
            String sql = "DELETE FROM DEPARTMENT WHERE DEPT_ID = "+id;
            int rowCount = statement.executeUpdate(sql);
            return rowCount;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return 0;
        }
    }


}
