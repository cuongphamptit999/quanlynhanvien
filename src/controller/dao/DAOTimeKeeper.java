/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Department;
import model.Timekeeper;

/**
 *
 * @author Cuong Pham
 */
public class DAOTimeKeeper extends IDAO<Timekeeper>{

    public DAOTimeKeeper(Connection conn) {
        this.conn = conn;
        try {
            this.statement = this.conn.createStatement();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    

    @Override
    public List<Timekeeper> selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Timekeeper> selectByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int insert(Timekeeper timekeeper) {
       String sql = "INSERT INTO TIMEKEEPER (Date_Time, In_Out, EMP_ID) VALUES (?,?,?)";
        try {
            this.preStatement = this.conn.prepareStatement(sql);
            this.preStatement.setTimestamp(1, new Timestamp(timekeeper.getDate_Time().getTime()));
            this.preStatement.setBoolean(2, timekeeper.isIn_Out());
            this.preStatement.setInt(3, timekeeper.getEmpId());
            
            int rowCount = this.preStatement.executeUpdate();
            return rowCount;
        } catch (SQLException ex) {
            Logger.getLogger(DAODepartment.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    public List<Timekeeper> selectByIdEmployee(int id){
        List<Timekeeper> result = new ArrayList<>();
        try {
            String sql = "SELECT * FROM TIMEKEEPER WHERE EMP_ID = "+id;
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                Timekeeper timekeeper = new Timekeeper(rs.getInt(1), rs.getTimestamp(2), rs.getBoolean(3), rs.getInt(4));
                result.add(timekeeper);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        return result;
    }

    @Override
    public int update(Timekeeper timekeeper) {
        String sql = "UPDATE TIMEKEEPER SET Date_Time = ?, In_Out = ?, EMP_ID = ? WHERE Timekeeper_Id = ?";
        try {
            this.preStatement = this.conn.prepareStatement(sql);
            this.preStatement.setTimestamp(1, new Timestamp(timekeeper.getDate_Time().getTime()));
            this.preStatement.setBoolean(2, timekeeper.isIn_Out());
            this.preStatement.setInt(3, timekeeper.getEmpId());
            this.preStatement.setInt(4, timekeeper.getTimekeeper_Id());
            
            int rowCount = this.preStatement.executeUpdate();
            return rowCount;
        } catch (SQLException ex) {
            Logger.getLogger(DAODepartment.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public int delete(int id) {
        try {
            String sql = "DELETE FROM TIMEKEEPER WHERE Timekeeper_Id = "+id;
            int rowCount = statement.executeUpdate(sql);
            return rowCount;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public void closeConnection() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
