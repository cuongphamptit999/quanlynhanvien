/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.department;

import controller.dao.DAODepartment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import javax.swing.JOptionPane;
import model.Department;
import view.department.AddDepartmentView;
import view.department.ManageDepartmentView;

/**
 *
 * @author Admin
 */
public class AddDepartmentController {

    private DAODepartment dAODepartment;
    private Connection conn;
    private AddDepartmentView addDepartmentView;

    public AddDepartmentController(Connection conn, AddDepartmentView addDepartmentView) {
        this.conn = conn;
        this.dAODepartment = new DAODepartment(conn);
        this.addDepartmentView = addDepartmentView;
        this.addDepartmentView.addListener(new AddDepartmentListener());
    }

    class AddDepartmentListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == addDepartmentView.getjButton1()) {
                Department department = new Department();
                department.setDeptName(addDepartmentView.getjTextField1().getText());
                department.setDeptNo(addDepartmentView.getjTextField2().getText());
                department.setLocation(addDepartmentView.getjTextField3().getText());

                int t = dAODepartment.insert(department);
                if (t > 0) {
                    JOptionPane.showMessageDialog(addDepartmentView, "Bạn đã thêm phòng ban thành công");
                    addDepartmentView.dispose();
                    ManageDepartmentView manageDepartmentView = new ManageDepartmentView();
                    ManageDepartmentController manageEmployeeController = new ManageDepartmentController(manageDepartmentView, conn);
                    manageDepartmentView.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(addDepartmentView, "Bạn đã thêm phòng ban thất bại");
                }

            }

            if (e.getSource() == addDepartmentView.getjButton2()) {
                addDepartmentView.dispose();
                ManageDepartmentView manageDepartmentView = new ManageDepartmentView();
                ManageDepartmentController manageEmployeeController = new ManageDepartmentController(manageDepartmentView, conn);
                manageDepartmentView.setVisible(true);
            }
        }

    }
}
