package controller.department;

import controller.dao.DAODepartment;
import controller.dao.DAOEmployee;
import controller.dao.DAOSalaryGrade;
import controller.employee.EditEmployeeController;
import controller.employee.ManageEmployeeController;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.JOptionPane;
import model.Department;
import model.Employee;
import model.SalaryGrade;
import view.department.EditDepartmentView;
import view.department.ManageDepartmentView;
import view.employee.EditEmployeeView;
import view.employee.ManageEmployeeView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author haima
 */
public class EditDepartmentController {

    private DAODepartment dAODepartment;
    private EditDepartmentView editDepartmentView;
    private Connection conn;
    private Department department;

    public EditDepartmentController(Connection conn, EditDepartmentView editEmployeeView, Department department) {
        this.conn = conn;
        this.dAODepartment = new DAODepartment(conn);
        this.department = department;
        this.editDepartmentView = editEmployeeView;
        this.editDepartmentView.addListener(new EditDepartmentListener());
        setDepartmentInEditView();
    }

    private void setDepartmentInEditView() {
        editDepartmentView.getjTextField3().setText(department.getDeptName());
        editDepartmentView.getjTextField4().setText(department.getDeptNo());
        editDepartmentView.getjTextField5().setText(department.getLocation());
    }

    class EditDepartmentListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == editDepartmentView.getjButton1()) {

                department.setDeptName(editDepartmentView.getjTextField3().getText());
                department.setDeptNo(editDepartmentView.getjTextField4().getText());
                department.setLocation(editDepartmentView.getjTextField5().getText());

                int t = dAODepartment.update(department);
                if (t > 0) {
                    JOptionPane.showMessageDialog(editDepartmentView, "Bạn đã sửa thành công");
                    editDepartmentView.dispose();
                    ManageDepartmentView manageDepartmentView = new ManageDepartmentView();
                    ManageDepartmentController manageDepartmentController = new ManageDepartmentController(manageDepartmentView, conn);
                    manageDepartmentView.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(editDepartmentView, "Bạn đã sửa thất bại");
                }
            }
            if (e.getSource() == editDepartmentView.getjButton2()) {
                editDepartmentView.dispose();
                ManageDepartmentView manageDepartmentView = new ManageDepartmentView();
                ManageDepartmentController manageDepartmentController = new ManageDepartmentController(manageDepartmentView, conn);
                manageDepartmentView.setVisible(true);
            }
        }
    }
}
