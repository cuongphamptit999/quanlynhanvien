/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.department;

import controller.ManageController;
import controller.dao.DAODepartment;
import controller.dao.DAOEmployee;
import controller.dao.DAOSalaryGrade;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import model.Department;
import view.department.ManageDepartmentView;
import view.ManageView;
import view.department.AddDepartmentView;
import view.department.EditDepartmentView;
import view.department.ListDepartmentView;
import view.department.SearchDepartmentView;

/**
 *
 * @author Admin
 */
public class ManageDepartmentController {

    private DAOEmployee dAOEmployee;
    private DAODepartment dAODepartment;
    private DAOSalaryGrade dAOSalaryGrade;
    private ManageDepartmentView manageDepartmentView;
    private Connection conn;

    public ManageDepartmentController(ManageDepartmentView manageDepartmentView, Connection conn) {
        this.manageDepartmentView = manageDepartmentView;
        this.manageDepartmentView.addListener(new ManageDepartmentListener());

        this.conn = conn;
        dAOEmployee = new DAOEmployee(conn);
        dAODepartment = new DAODepartment(conn);
        dAOSalaryGrade = new DAOSalaryGrade(conn);
    }

    class ManageDepartmentListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == manageDepartmentView.getjButton3()) {
                manageDepartmentView.dispose();
                ListDepartmentView listDepartmentView = new ListDepartmentView();
                DefaultTableModel model = (DefaultTableModel) listDepartmentView.getjTable1().getModel();
                model.setRowCount(0);
                List<Department> list = dAODepartment.selectAll();
                for (Department department : list) {
                    model.addRow(department.toObject());
                }
                ListDepartmentController listDepartmentController = new ListDepartmentController(listDepartmentView, conn);
                listDepartmentView.setVisible(true);
            }

            if (e.getSource() == manageDepartmentView.getjButton1()) {
                manageDepartmentView.dispose();
                List<Department> departments = dAODepartment.selectAll();
                AddDepartmentView addDepartmentView = new AddDepartmentView();
                AddDepartmentController addDepartmentController = new AddDepartmentController(conn, addDepartmentView);
                addDepartmentView.setVisible(true);

            }

            if (e.getSource() == manageDepartmentView.getjButton2()) {
                manageDepartmentView.dispose();
                SearchDepartmentView searchDepartmentView = new SearchDepartmentView();
                SearchDepartmentController searchDepartmentController = new SearchDepartmentController(searchDepartmentView, conn);
                searchDepartmentView.setVisible(true);
            }

            if (e.getSource() == manageDepartmentView.getjButton4()) {
                manageDepartmentView.dispose();
                ManageView manageView = new ManageView();
                ManageController manageController = new ManageController(manageView, conn);
                manageView.setVisible(true);
            }
        }

    }
}
