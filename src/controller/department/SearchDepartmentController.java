/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.department;

import controller.dao.DAODepartment;
import controller.employee.ManageEmployeeController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import model.Department;
import model.Employee;
import view.department.EditDepartmentView;
import view.department.ListDepartmentView;
import view.department.ManageDepartmentView;
import view.department.SearchDepartmentView;
import view.employee.ManageEmployeeView;

/**
 *
 * @author Cuong Pham
 */
public class SearchDepartmentController {

    private SearchDepartmentView searchDepartmentView;
    private Connection conn;
    private DAODepartment dAODepartment;

    public SearchDepartmentController(SearchDepartmentView searchDepartmentView, Connection conn) {
        this.searchDepartmentView = searchDepartmentView;
        this.conn = conn;
        this.dAODepartment = new DAODepartment(conn);
        this.searchDepartmentView.addListener(new SearchDepartmentListener());

        searchDepartmentView.getjTable1().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e); //To change body of generated methods, choose Tools | Templates.
                int r = searchDepartmentView.getjTable1().getSelectedRow();
                int id = Integer.parseInt(searchDepartmentView.getjTable1().getValueAt(r, 0).toString());
                String name = searchDepartmentView.getjTable1().getValueAt(r, 1).toString();
                String no = searchDepartmentView.getjTable1().getValueAt(r, 2).toString();
                String location = searchDepartmentView.getjTable1().getValueAt(r, 3).toString();
                Department department = new Department(id, name, no, location);
                searchDepartmentView.dispose();
                EditDepartmentView editDepartmentView = new EditDepartmentView();
                EditDepartmentController DepartmentController = new EditDepartmentController(conn, editDepartmentView, department);
                editDepartmentView.setVisible(true);
            }
        });
    }

    class SearchDepartmentListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == searchDepartmentView.getjButton1()) {
                DefaultTableModel model = (DefaultTableModel) searchDepartmentView.getjTable1().getModel();
                model.setRowCount(0);
                List<Department> list = dAODepartment.selectByName(searchDepartmentView.getjTextField1().getText());
                for (Department department : list) {
                    model.addRow(department.toObject());
                }
            }

            if (e.getSource() == searchDepartmentView.getjButton2()) {
                searchDepartmentView.dispose();
                ManageDepartmentView manageDepartmentView = new ManageDepartmentView();
                ManageDepartmentController manageDepartmentController = new ManageDepartmentController(manageDepartmentView, conn);
                manageDepartmentView.setVisible(true);
            }

        }

    }

}
