/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.employee;

import controller.dao.DAODepartment;
import controller.dao.DAOEmployee;
import controller.dao.DAOSalaryGrade;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Department;
import model.Employee;
import model.SalaryGrade;
import view.employee.AddEmployeeView;
import view.employee.EditEmployeeView;
import view.employee.ListEmployeeView;
import view.employee.ManageEmployeeView;

/**
 *
 * @author Cuong Pham
 */
public class EditEmployeeController {

    private DAOEmployee dAOEmployee;
    private Connection conn;
    private EditEmployeeView editEmployeeView;
    private Employee employee;
    private DAODepartment dAODepartment;
    private DAOSalaryGrade dAOSalaryGrade;

    public EditEmployeeController(Connection conn, EditEmployeeView editEmployeeView, Employee employee) {
        this.conn = conn;
        this.dAOEmployee = new DAOEmployee(conn);
        dAODepartment = new DAODepartment(conn);
        dAOSalaryGrade = new DAOSalaryGrade(conn);

        this.editEmployeeView = editEmployeeView;
        this.editEmployeeView.addListener(new EditEmployeeListener());
        this.employee = employee;
        setEmployeeInEditView();

    }

    private void setEmployeeInEditView() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        editEmployeeView.getjTextField1().setText(employee.getEmpName());
        editEmployeeView.getjTextField2().setText(employee.getEmpNo());
        editEmployeeView.getjTextField3().setText(sdf.format(employee.getHireDate()));
        editEmployeeView.getjTextField4().setText(employee.getJob());
        editEmployeeView.getjTextField5().setText(employee.getSalary() + "");
        editEmployeeView.getjTextField6().setText(employee.getMngId() + "");
        List<Department> departments = dAODepartment.selectAll();
        List<SalaryGrade> salaryGrades = dAOSalaryGrade.selectAll();

        editEmployeeView.getjComboBox1().removeAllItems();
        editEmployeeView.getjComboBox2().removeAllItems();

        for (Department department : departments) {
            editEmployeeView.getjComboBox1().addItem(department.getDeptId() + "");
        }
        for (SalaryGrade salaryGrade : salaryGrades) {
            editEmployeeView.getjComboBox2().addItem(salaryGrade.getGrade() + "");
        }
        editEmployeeView.getjComboBox1().setSelectedItem(employee.getDeptId() + "");
        editEmployeeView.getjComboBox2().setSelectedItem(employee.getGrdId() + "");

    }

    class EditEmployeeListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == editEmployeeView.getjButton1()) {
                employee.setEmpName(editEmployeeView.getjTextField1().getText());
                employee.setEmpNo(editEmployeeView.getjTextField2().getText());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    employee.setHireDate(sdf.parse(editEmployeeView.getjTextField3().getText()));
                } catch (ParseException ex) {
                    Logger.getLogger(EditEmployeeController.class.getName()).log(Level.SEVERE, null, ex);
                }
                employee.setJob(editEmployeeView.getjTextField4().getText());
                employee.setSalary(Float.parseFloat(editEmployeeView.getjTextField5().getText()));
                employee.setImage(null);
                employee.setMngId(Integer.parseInt(editEmployeeView.getjTextField6().getText()));
                employee.setDeptId(Integer.parseInt(editEmployeeView.getjComboBox1().getSelectedItem().toString()));
                employee.setGrdId(Integer.parseInt(editEmployeeView.getjComboBox2().getSelectedItem().toString()));

                int t = dAOEmployee.update(employee);
                if (t > 0) {
                    JOptionPane.showMessageDialog(editEmployeeView, "Bạn đã sửa nhân viên thành công");

                    editEmployeeView.dispose();
                    ManageEmployeeView manageEmployeeView = new ManageEmployeeView();
                    ManageEmployeeController manageEmployeeController = new ManageEmployeeController(manageEmployeeView, conn);
                    manageEmployeeView.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(editEmployeeView, "Bạn đã sửa nhân viên thất bại");
                }

            }

            if (e.getSource() == editEmployeeView.getjButton2()) {
                editEmployeeView.dispose();
                ManageEmployeeView manageEmployeeView = new ManageEmployeeView();
                ManageEmployeeController manageEmployeeController = new ManageEmployeeController(manageEmployeeView, conn);
                manageEmployeeView.setVisible(true);
            }

        }

    }

}
