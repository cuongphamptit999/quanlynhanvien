/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.employee;

import controller.dao.DAOEmployee;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Employee;
import view.employee.EditEmployeeView;
import view.employee.ManageEmployeeView;
import view.employee.SearchEmployeeView;

/**
 *
 * @author Cuong Pham
 */
public class SearchEmployeeController {

    private Connection conn;
    private DAOEmployee dAOEmployee;
    private SearchEmployeeView searchEmployeeView;

    public SearchEmployeeController(boolean isDelete, Connection conn, SearchEmployeeView searchEmployeeView) {
        this.conn = conn;
        this.dAOEmployee = new DAOEmployee(conn);
        this.searchEmployeeView = searchEmployeeView;
        this.searchEmployeeView.addListener(new SearchEmployeeListener());

        if (isDelete) {
            searchEmployeeView.getjTable1().addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e); //To change body of generated methods, choose Tools | Templates.
                    int r = searchEmployeeView.getjTable1().getSelectedRow();
                    int id = Integer.parseInt(searchEmployeeView.getjTable1().getValueAt(r, 0).toString());
                    int t = dAOEmployee.delete(id);
                    if (t > 0) {
                        JOptionPane.showMessageDialog(searchEmployeeView, "Bạn đã xóa nhân viên thành công");
                        searchEmployeeView.dispose();
                        ManageEmployeeView manageEmployeeView = new ManageEmployeeView();
                        ManageEmployeeController manageEmployeeController = new ManageEmployeeController(manageEmployeeView, conn);
                        manageEmployeeView.setVisible(true);
                    } else {
                        JOptionPane.showMessageDialog(searchEmployeeView, "Bạn đã xóa nhân viên thất bại");
                    }
                }
            });
        } else {
            searchEmployeeView.getjTable1().addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e); //To change body of generated methods, choose Tools | Templates.
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    int r = searchEmployeeView.getjTable1().getSelectedRow();
                    int id = Integer.parseInt(searchEmployeeView.getjTable1().getValueAt(r, 0).toString());
                    String empName = searchEmployeeView.getjTable1().getValueAt(r, 1).toString();
                    String empNo = searchEmployeeView.getjTable1().getValueAt(r, 2).toString();
                    Date hireDate = null;
                    try {
                        hireDate = sdf.parse(searchEmployeeView.getjTable1().getValueAt(r, 3).toString());
                    } catch (ParseException ex) {
                        Logger.getLogger(SearchEmployeeController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    String job = searchEmployeeView.getjTable1().getValueAt(r, 5).toString();
                    float salary = Float.parseFloat(searchEmployeeView.getjTable1().getValueAt(r, 6).toString());
                    int deptId = Integer.parseInt(searchEmployeeView.getjTable1().getValueAt(r, 7).toString());
                    int mngId = Integer.parseInt(searchEmployeeView.getjTable1().getValueAt(r, 8).toString());
                    int grdId = Integer.parseInt(searchEmployeeView.getjTable1().getValueAt(r, 9).toString());

                    Employee employee = new Employee(id, empName, empNo, hireDate, null, job, salary, deptId, mngId, grdId);

                    searchEmployeeView.dispose();
                    EditEmployeeView editEmployeeView = new EditEmployeeView();
                    EditEmployeeController editEmployeeController = new EditEmployeeController(conn, editEmployeeView, employee);
                    editEmployeeView.setVisible(true);
                }
            });
        }

    }

    class SearchEmployeeListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == searchEmployeeView.getjButton1()) {
                DefaultTableModel model = (DefaultTableModel) searchEmployeeView.getjTable1().getModel();
                model.setRowCount(0);
                List<Employee> list = dAOEmployee.selectByName(searchEmployeeView.getjTextField1().getText());
                for (Employee employee : list) {
                    model.addRow(employee.toObject());
                }
            }

            if (e.getSource() == searchEmployeeView.getjButton2()) {
                searchEmployeeView.dispose();
                ManageEmployeeView manageEmployeeView = new ManageEmployeeView();
                ManageEmployeeController manageEmployeeController = new ManageEmployeeController(manageEmployeeView, conn);
                manageEmployeeView.setVisible(true);
            }

        }

    }

}
