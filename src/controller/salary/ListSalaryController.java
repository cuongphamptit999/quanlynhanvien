/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.salary;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import view.salary.ListSalaryView;
import view.salary.ManagerSalaryView;

/**
 *
 * @author Admin
 */
public class ListSalaryController {

    private ListSalaryView listSalaryView;
    private Connection conn;

    public ListSalaryController(ListSalaryView listSalaryView, Connection conn) {
        this.listSalaryView = listSalaryView;
        this.listSalaryView.addListener(new ListSalaryListener());
        this.conn = conn;
    }

    class ListSalaryListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            listSalaryView.dispose();
            ManagerSalaryView managerSalaryView = new ManagerSalaryView();
            ManagerSalaryController managerSalaryController = new ManagerSalaryController(managerSalaryView, conn);
            managerSalaryView.setVisible(true);

        }

    }
}
