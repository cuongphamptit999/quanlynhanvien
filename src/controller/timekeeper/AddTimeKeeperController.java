/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.timekeeper;

import controller.dao.DAOTimeKeeper;
import controller.employee.AddEmployeeController;
import controller.employee.ManageEmployeeController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Employee;
import model.Timekeeper;
import view.employee.ManageEmployeeView;
import view.timekeeper.AddTimeKeeperView;
import view.timekeeper.ManageTimeKeeperView;

/**
 *
 * @author Cuong Pham
 */
public class AddTimeKeeperController {

    private AddTimeKeeperView addTimeKeeperView;
    private Connection conn;
    private DAOTimeKeeper dAOTimeKeeper;

    public AddTimeKeeperController(AddTimeKeeperView addTimeKeeperView, Connection conn) {
        this.addTimeKeeperView = addTimeKeeperView;
        this.conn = conn;
        this.dAOTimeKeeper = new DAOTimeKeeper(conn);
        this.addTimeKeeperView.addListener(new AddTimeKeeperListener());
    }

    class AddTimeKeeperListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == addTimeKeeperView.getjButton1()) {
                Timekeeper timekeeper = new Timekeeper();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    timekeeper.setDate_Time(sdf.parse(addTimeKeeperView.getjTextField1().getText()));
                } catch (ParseException ex) {
                    Logger.getLogger(AddEmployeeController.class.getName()).log(Level.SEVERE, null, ex);
                }
                timekeeper.setEmpId(Integer.parseInt(addTimeKeeperView.getjComboBox1().getSelectedItem().toString()));
                if (addTimeKeeperView.getjRadioButton1().isSelected()) {
                    timekeeper.setIn_Out(true);
                } else {
                    timekeeper.setIn_Out(false);
                }

                int t = dAOTimeKeeper.insert(timekeeper);
                if (t > 0) {
                    JOptionPane.showMessageDialog(addTimeKeeperView, "Bạn đã thêm lịch làm việc thành công");

                    addTimeKeeperView.dispose();
                    ManageTimeKeeperView manageTimeKeeperView = new ManageTimeKeeperView();
                    ManageTimeKeeperController manageTimeKeeperController = new ManageTimeKeeperController(conn, manageTimeKeeperView);
                    manageTimeKeeperView.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(addTimeKeeperView, "Bạn đã thêm lịch làm việc thất bại");
                }

            }

            if (e.getSource() == addTimeKeeperView.getjButton2()) {
                addTimeKeeperView.dispose();
                ManageTimeKeeperView manageTimeKeeperView = new ManageTimeKeeperView();
                ManageTimeKeeperController manageTimeKeeperController = new ManageTimeKeeperController(conn, manageTimeKeeperView);
                manageTimeKeeperView.setVisible(true);
            }

        }

    }

}
