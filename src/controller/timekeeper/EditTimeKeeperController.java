/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.timekeeper;

import controller.dao.DAOEmployee;
import controller.dao.DAOTimeKeeper;
import controller.employee.AddEmployeeController;
import controller.employee.ManageEmployeeController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Employee;
import model.Timekeeper;
import view.employee.ManageEmployeeView;
import view.timekeeper.AddTimeKeeperView;
import view.timekeeper.EditTimeKeeperView;
import view.timekeeper.ManageTimeKeeperView;

/**
 *
 * @author Cuong Pham
 */
public class EditTimeKeeperController {

    private EditTimeKeeperView editTimeKeeperView;
    private Connection conn;
    private DAOTimeKeeper dAOTimeKeeper;
    private Timekeeper timekeeper;

    public EditTimeKeeperController(EditTimeKeeperView editTimeKeeperView, Connection conn, Timekeeper timekeeper) {
        this.editTimeKeeperView = editTimeKeeperView;
        this.conn = conn;
        this.dAOTimeKeeper = new DAOTimeKeeper(conn);
        this.editTimeKeeperView.addListener(new EditTimeKeeperListener());
        this.timekeeper = timekeeper;
        setTimeKeeperInEditView();
    }

    private void setTimeKeeperInEditView() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        editTimeKeeperView.getjTextField1().setText(sdf.format(timekeeper.getDate_Time()));
        if (timekeeper.isIn_Out()) {
            editTimeKeeperView.getjRadioButton1().setSelected(true);
        } else {
            editTimeKeeperView.getjRadioButton2().setSelected(true);
        }

    }

    class EditTimeKeeperListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == editTimeKeeperView.getjButton1()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    timekeeper.setDate_Time(sdf.parse(editTimeKeeperView.getjTextField1().getText()));
                } catch (ParseException ex) {
                    Logger.getLogger(AddEmployeeController.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (editTimeKeeperView.getjRadioButton1().isSelected()) {
                    timekeeper.setIn_Out(true);
                } else {
                    timekeeper.setIn_Out(false);
                }

                int t = dAOTimeKeeper.update(timekeeper);
                if (t > 0) {
                    JOptionPane.showMessageDialog(editTimeKeeperView, "Bạn đã sửa lịch làm việc thành công");
                    editTimeKeeperView.dispose();
                    ManageTimeKeeperView manageTimeKeeperView = new ManageTimeKeeperView();
                    ManageTimeKeeperController manageTimeKeeperController = new ManageTimeKeeperController(conn, manageTimeKeeperView);
                    manageTimeKeeperView.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(editTimeKeeperView, "Bạn đã sửa lịch làm việc thất bại");
                }

            }

            if (e.getSource() == editTimeKeeperView.getjButton2()) {
                editTimeKeeperView.dispose();
                ManageTimeKeeperView manageTimeKeeperView = new ManageTimeKeeperView();
                ManageTimeKeeperController manageTimeKeeperController = new ManageTimeKeeperController(conn, manageTimeKeeperView);
                manageTimeKeeperView.setVisible(true);
            }

        }

    }

}
