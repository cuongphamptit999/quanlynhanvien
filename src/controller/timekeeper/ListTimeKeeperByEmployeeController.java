/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.timekeeper;

import controller.employee.*;
import controller.ManageController;
import controller.dao.DAOTimeKeeper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Employee;
import model.Timekeeper;
import view.ManageView;
import view.employee.EditEmployeeView;
import view.employee.ListEmployeeView;
import view.employee.ManageEmployeeView;
import view.timekeeper.EditTimeKeeperView;
import view.timekeeper.ListTimeKeeperByEmployeeView;
import view.timekeeper.ManageTimeKeeperView;

/**
 *
 * @author Cuong Pham
 */
public class ListTimeKeeperByEmployeeController {

    private ListTimeKeeperByEmployeeView listTimeKeeperByEmployeeView;
    private Connection conn;
    private DAOTimeKeeper dAOTimeKeeper;

    public ListTimeKeeperByEmployeeController(ListTimeKeeperByEmployeeView listTimeKeeperByEmployeeView, Connection conn, int flag) {
        this.listTimeKeeperByEmployeeView = listTimeKeeperByEmployeeView;
        this.listTimeKeeperByEmployeeView.addListener(new ListTimeKeeperByEmployeeListener());
        this.dAOTimeKeeper = new DAOTimeKeeper(conn);
        this.conn = conn;

        if (flag == 1) {
            listTimeKeeperByEmployeeView.getjTable1().addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e); //To change body of generated methods, choose Tools | Templates.
                    int r = listTimeKeeperByEmployeeView.getjTable1().getSelectedRow();
                    int id = Integer.parseInt(listTimeKeeperByEmployeeView.getjTable1().getValueAt(r, 0).toString());
                    int t = dAOTimeKeeper.delete(id);
                    if (t > 0) {
                        JOptionPane.showMessageDialog(listTimeKeeperByEmployeeView, "Bạn đã xóa lịch thành công");
                        listTimeKeeperByEmployeeView.dispose();
                        ManageTimeKeeperView manageTimeKeeperView = new ManageTimeKeeperView();
                        ManageTimeKeeperController manageTimeKeeperController = new ManageTimeKeeperController(conn, manageTimeKeeperView);
                        manageTimeKeeperView.setVisible(true);
                    } else {
                        JOptionPane.showMessageDialog(listTimeKeeperByEmployeeView, "Bạn đã xóa lịch thất bại");
                    }
                }
            });
        } else if (flag == 0) {
            listTimeKeeperByEmployeeView.getjTable1().addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e); //To change body of generated methods, choose Tools | Templates.
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    int r = listTimeKeeperByEmployeeView.getjTable1().getSelectedRow();
                    int id = Integer.parseInt(listTimeKeeperByEmployeeView.getjTable1().getValueAt(r, 0).toString());
                    Date date_Time = null;
                    try {
                        date_Time = sdf.parse(listTimeKeeperByEmployeeView.getjTable1().getValueAt(r, 1).toString());
                    } catch (ParseException ex) {
                        Logger.getLogger(SearchEmployeeController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    boolean in_Out = Boolean.parseBoolean(listTimeKeeperByEmployeeView.getjTable1().getValueAt(r, 2).toString());
                    int empId = Integer.parseInt(listTimeKeeperByEmployeeView.getjTable1().getValueAt(r, 3).toString());

                    Timekeeper timekeeper = new Timekeeper(id, date_Time, in_Out, empId);

                    listTimeKeeperByEmployeeView.dispose();
                    EditTimeKeeperView editTimeKeeperView = new EditTimeKeeperView();
                    EditTimeKeeperController editTimeKeeperController = new EditTimeKeeperController(editTimeKeeperView, conn, timekeeper);
                    editTimeKeeperView.setVisible(true);
                }
            });
        }
    }

    class ListTimeKeeperByEmployeeListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            listTimeKeeperByEmployeeView.dispose();
            ManageTimeKeeperView manageTimeKeeperView = new ManageTimeKeeperView();
            ManageTimeKeeperController manageTimeKeeperController = new ManageTimeKeeperController(conn, manageTimeKeeperView);
            manageTimeKeeperView.setVisible(true);

        }

    }

}
