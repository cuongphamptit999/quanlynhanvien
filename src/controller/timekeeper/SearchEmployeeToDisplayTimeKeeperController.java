/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.timekeeper;

import controller.employee.*;
import controller.dao.DAOEmployee;
import controller.dao.DAOTimeKeeper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Employee;
import model.Timekeeper;
import view.employee.EditEmployeeView;
import view.employee.ListEmployeeView;
import view.employee.ManageEmployeeView;
import view.employee.SearchEmployeeView;
import view.timekeeper.ListTimeKeeperByEmployeeView;

/**
 *
 * @author Cuong Pham
 */
public class SearchEmployeeToDisplayTimeKeeperController {

    private Connection conn;
    private DAOEmployee dAOEmployee;
    private SearchEmployeeView searchEmployeeView;
    private DAOTimeKeeper dAOTimeKeeper;

    public SearchEmployeeToDisplayTimeKeeperController(Connection conn, SearchEmployeeView searchEmployeeView, int flag) {
        this.conn = conn;
        this.dAOEmployee = new DAOEmployee(conn);
        this.dAOTimeKeeper = new DAOTimeKeeper(conn);
        this.searchEmployeeView = searchEmployeeView;
        this.searchEmployeeView.addListener(new SearchEmployeeListener());

        searchEmployeeView.getjTable1().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e); //To change body of generated methods, choose Tools | Templates.
                int r = searchEmployeeView.getjTable1().getSelectedRow();
                int id = Integer.parseInt(searchEmployeeView.getjTable1().getValueAt(r, 0).toString());

                searchEmployeeView.dispose();
                ListTimeKeeperByEmployeeView listTimeKeeperByEmployeeView = new ListTimeKeeperByEmployeeView();
                DefaultTableModel model = (DefaultTableModel) listTimeKeeperByEmployeeView.getjTable1().getModel();
                model.setRowCount(0);
                List<Timekeeper> list = dAOTimeKeeper.selectByIdEmployee(id);
                for (Timekeeper timekeeper : list) {
                    model.addRow(timekeeper.toObject());
                }
                ListTimeKeeperByEmployeeController listTimeKeeperByEmployeeController = new ListTimeKeeperByEmployeeController(listTimeKeeperByEmployeeView, conn, flag);
                listTimeKeeperByEmployeeView.setVisible(true);

                searchEmployeeView.dispose();

            }
        });

    }

    class SearchEmployeeListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == searchEmployeeView.getjButton1()) {

                DefaultTableModel model = (DefaultTableModel) searchEmployeeView.getjTable1().getModel();
                model.setRowCount(0);
                List<Employee> list = dAOEmployee.selectByName(searchEmployeeView.getjTextField1().getText());
                for (Employee employee : list) {
                    model.addRow(employee.toObject());
                }

            }

            if (e.getSource() == searchEmployeeView.getjButton2()) {
                searchEmployeeView.dispose();
                ManageEmployeeView manageEmployeeView = new ManageEmployeeView();
                ManageEmployeeController manageEmployeeController = new ManageEmployeeController(manageEmployeeView, conn);
                manageEmployeeView.setVisible(true);
            }

        }

    }

}
