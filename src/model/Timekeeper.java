package model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 * The persistent class for the TIMEKEEPER database table.
 *
 */
public class Timekeeper implements Serializable {

    private static final long serialVersionUID = 1L;

    public int getTimekeeper_Id() {
        return timekeeper_Id;
    }

    public void setTimekeeper_Id(int timekeeper_Id) {
        this.timekeeper_Id = timekeeper_Id;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public boolean isIn_Out() {
        return in_Out;
    }

    public void setIn_Out(boolean in_Out) {
        this.in_Out = in_Out;
    }

    private int timekeeper_Id;

    private Date date_Time;

    private int empId;

    private boolean in_Out;

    public Timekeeper(int timekeeper_Id, Date date_Time, boolean in_Out, int empId) {
        this.timekeeper_Id = timekeeper_Id;
        this.date_Time = date_Time;
        this.empId = empId;
        this.in_Out = in_Out;
    }

    public Timekeeper() {
    }

    public Date getDate_Time() {
        return this.date_Time;
    }

    public void setDate_Time(Date date_Time) {
        this.date_Time = date_Time;
    }

    public Object[] toObject() {
        return new Object[]{timekeeper_Id, date_Time, in_Out, empId};
    }

}
